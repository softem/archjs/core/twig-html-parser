import * as Twig from "../dist/TwigHtmlParser";

describe('TwigHtmlParser', function () {

    describe('generate ast from HTML', function(){
        var twig = new Twig.Parser();

        it('one element <div></div>',function () {
            var ast  = twig.parse( '<div></div>' );
            expect(ast.childNodes[0].type).toEqual('HtmlElementNode');
            expect(ast.childNodes[0].nodeName ).toEqual('div');
        });

        it('n nested elements element &lt;el_01&gt;&lt;el_11&gt; ... &lt;/el_11&gt;&lt;/el_01&gt;',function () {
            var input_begin =   '';
            var input_end   =   '';
            var input       =   '';
            var ast         = null;
            var length      =  100;
            var count       =   -1;
            var tmp         = null;
            for( var i = 0; i<length; i++){
                input_begin += "<div"+i+">";
                input_end   += "</div"+i+">";
            }
            input   = input_begin + input_end;
            ast     = twig.parse( input );
            count   = -1; //ignore root
            tmp     = ast;
            while( tmp ){
                tmp = tmp.childNodes[0];
                count++;
            }
            expect(count).toEqual(length);
        });

        it('n neighbor elements element &lt;el_1&gt;&lt;/el_1&gt; &lt;el_2&gt;&lt;/el_2&gt;...&lt;el_n&gt;&lt;/el_n&gt;',function () {
            var input_begin =   '';
            var input_end   =   '';
            var input       =   '';
            var ast         = null;
            var length      =  100;
            for( var i = 0; i<length; i++){
                input_begin += "<div"+i+"></div"+i+">";
            }
            input   = input_begin + input_end;
            ast     = twig.parse( input );
            expect(ast.childNodes.length).toEqual(length);
        });
    });

    describe('generate ast from twig', function() {
        describe('IfStatement', function () {
            var twig = new Twig.Parser();
            it('{% if 1 %}{% endif %}', function () {
                var ast = twig.parse('{%if 1%}{%endif%}');
                expect(ast.childNodes[0].type).toEqual('IfStatement');
                expect(ast.childNodes[0].condition).toEqual(1);
            });
            it('{% if 1==2 %}{% endif %}', function () {
                var ast = twig.parse('{%if 1==2 %}{%endif%}');
                expect(ast.childNodes[0].type).toEqual('IfStatement');
                expect(ast.childNodes[0].condition.type).toEqual('operation');
                expect(ast.childNodes[0].condition.operator).toEqual('==');
                expect(ast.childNodes[0].condition.left).toEqual(1);
                expect(ast.childNodes[0].condition.right).toEqual(2);
            });
        })
    });
});
