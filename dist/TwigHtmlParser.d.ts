interface ICursor {
    first_line?     :number;
    first_column?   :number;
    last_line?      :number;
    last_column?    :number;
    range?          :number[];
}
interface IAstNode {
    loc         :ICursor;
    type        :string;
    [key:string]:any;
}
interface IAttribute extends IAstNode {
    name    :string;
    value   :string;
}
interface IHtmlElementNode extends IAstNode {
    nodeName    :string;
    attributes  :IAttribute[];
    childNodes  :IAstNode[];
}



interface IIdenrifier extends IAstNode{
    name: string;
}
interface IMemberAccessor extends IAstNode{
    object:   IExpression;
    property: string|IExpression;
}

interface IOperation extends IUOperation {
    left:     string;
}
interface IUOperation extends IAstNode {
operator: string;
right:    string;
}
interface IArrayLiteral extends IAstNode {
    items: IExpression[];
}
interface IArrayRange extends IAstNode {
    from: number|string|boolean;
    to:   number|string|boolean;
}
interface ICallExpression extends IAstNode{
    //TODO describe interface
}
type IExpression =    ICallExpression
                    & IArrayLiteral
                    & IArrayRange
                    & IUOperation
                    & IOperation
                    & IMemberAccessor
                    & IIdenrifier
;

declare class Parser {
    yy:object;
    symbols_:{[key:string]:number};
    terminals_:{[key:number]:string};
    trace();
    productions_:any[];
    performAction( yytext, yyleng, yylineno, yy, yystate, $$, _$ );
    table:any[];
    defaultActions:object;
    parseError(str, hash);
    parse( input ):IHtmlElementNode;
}

export {
    ICursor
    ,IAstNode
    ,IAttribute
    ,IHtmlElementNode
    ,IIdenrifier
    ,IMemberAccessor
    ,IOperation
    ,IUOperation
    ,IArrayLiteral
    ,IArrayRange
    ,ICallExpression
    ,IExpression
    ,Parser
}