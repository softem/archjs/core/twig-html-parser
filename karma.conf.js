module.exports = function(config) {
    config.set({
        karmaTypescriptConfig:{
            tsconfig: "./tsconfig.json"
        },
        frameworks: ["jasmine", "karma-typescript"],
        files: [
            { pattern: "dist/**/*.ts" },
            { pattern: "dist/**/*.js" },
            { pattern: "test/**/*.ts" }
        ],
        preprocessors: {
            "dist/**/*.ts": ["karma-typescript"],
            "dist/**/*.js": ["karma-typescript","coverage"],
            "test/**/*.ts": ["karma-typescript"]
        },
        reporters: ["progress","spec-as-html", "coverage", "junit", "karma-typescript"],
        coverageReporter: {
            dir : 'report/coverage/'
        },
        specAsHtmlReporter : {
            dir :       "report",
            outputFile: "spec.html"
        },
        junitReporter: {
            outputDir:      "report",
            outputFile:     "junit.xml",
            useBrowserName: false
        },
        browsers: ["Chrome"]
    });
};