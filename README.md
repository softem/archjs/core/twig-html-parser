# twig-html-parser
**language:** javascript

**module type:** commonjs

**description:** 

This package generates an AST tree from html+twig template.

## Components of AST tree

### currsor 
```typescript
interface ICursor{
    first_line:number;
    first_column:number;
    last_line:number;
    last_column:number;
    "range":number[];
}
```

### IStatement
```typescript
interface IStatement{
    loc:ICursor;
    type:string;
    nodeName:string;
    condition:IExpression;
}
```

### if statement
node interface
```typescript
interface IIfStatement extends IStatement{
     type:      'LogicNode';
     nodeName:  'if';
     condition: IExpression;
}
```
twig code example:
```twig
{% if condition %}
  do something if true
{% endif %}
```

Generated AST node
```json
    {
      "loc": {
        "first_line":   1,
        "first_column": 0,
        "last_line":    1,
        "last_column": 18,
        "range":[ 0, 18 ]
      },
      "type": "LogicNode",
      "nodeName": "if",
      "condition": {
        "type": "Idenrifier",
        "name": "condition"
      },
      "childNodes": [
        {
          "loc": {
            "first_line":   1,
            "first_column":18,
            "last_line":    1,
            "last_column": 40,
            "range": [ 18, 40 ]
          },
          "type": "TextNode",
          "text": " do something if true "
        }
      ]
    }
```


### for statement
