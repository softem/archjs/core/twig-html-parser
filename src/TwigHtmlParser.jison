/* description: Parses end executes mathematical expressions. */

/* lexical grammar */
%lex
%options ranges
digit                       [0-9]
number                      {digit}+(\.{digit}+)?
char                        [a-zA-Z]
char_                       [a-zA-Z_]
char_digit_                 {char_}|\-|{digit}

html_tag_del_lt             [<]
html_tag_del_gt             [>]

html_tag_open_begin         {html_tag_del_lt}/{html_tag_name}
html_tag_close_begin        {html_tag_del_lt}[\/]/{html_tag_name}
html_tag_name               {char_}({char_digit_})*
html_tag_attr_name          {char_}({char_digit_})*
identifier                  {char_}({char_digit_})*

html_comment_begin          {html_tag_del_lt}'!--'
html_comment_text           .*?(?={html_comment_end})
html_comment_end            '--'{html_tag_del_gt}
html_comment                {html_comment_begin}{html_comment_text}{html_comment_end}

html_text_end               {html_tag_open_begin}|{html_tag_close_begin}|{html_comment_begin}|{twig_print_del_open}|{twig_logic_del_open}
html_text                   ((?!({html_text_end})).|\s)+


twig_print_del_open         '{{'
twig_print_del_close        '}}'

twig_logic_del_open         '{%'
twig_logic_del_close        '%}'

twig_comment_del_open       '{#'
twig_comment_del_close      '#}'

/*define lexer states*/
%x html
%x html_tag_open
%x html_tag_close
%x html_tag_attr
%x html_text
%x html_tag_attr_value
%x html_tag_attr_value_double_quote_string
%x html_tag_attr_value_single_quote_string

%x twig_print
%x twig_logic
%x html_tag_name
%%

<html_tag_close,html_tag_open,html_tag_attr,html_tag_attr_value,twig_print,twig_logic>\s+
      %{
        // ignore whitespace
      %}
<html>{html_comment}
      %{
        // ignore comments
      %}

<html>/{html_text}
      %{
        this.begin('html_text');
      %}
<html_text>{html_text}
      %{
        this.popState();
        if ( yytext.replace(/\s/g, '').length) {
          return 'HTML_TEXT';
        }
      %}

<html>{html_tag_open_begin}
      %{
        this.begin('html_tag_name');
        return '<';
      %}
<html_tag_name>{html_tag_name}
      %{
        this.popState();
        this.begin('html_tag_open');
        return 'TAG';
      %}
<html_tag_open>{html_tag_del_gt}
      %{
        this.popState();
        return '>';
      %}
<html_tag_open>[/]{html_tag_del_gt}
      %{
        this.popState();
        return '/>';
      %}

<html>{html_tag_close_begin}
      %{
        this.begin('html_tag_close');
        return '</';
      %}
<html_tag_close>{html_tag_name}
      %{
        return 'TAG';
      %}
<html_tag_close>{html_tag_del_gt}
      %{
        this.popState();
        return '>';
      %}

<html_tag_open>/{html_tag_attr_name}
      %{
        this.begin('html_tag_attr');
      %}
<html_tag_attr>{html_tag_attr_name}
      %{
        return 'HTML_TAG_ATTR_NAME';
      %}
<html_tag_attr>[=]
      %{
        this.begin('html_tag_attr_value');
        return '=';
      %}
<html_tag_attr_value>{number}
      %{
        this.popState();
        return 'HTML_TAG_ATTR_VALUE';
      %}
<html_tag_attr_value>[']
      %{
        this.popState();
        this.begin('html_tag_attr_value_single_quote_string');
      %}
<html_tag_attr_value_single_quote_string>[^']*[']
      %{
        this.popState();
        return 'HTML_TAG_ATTR_VALUE';
      %}
<html_tag_attr_value>["]
      %{
        this.popState();
        this.begin('html_tag_attr_value_double_quote_string');
      %}
<html_tag_attr_value_double_quote_string>[^"]*["]
      %{
        this.popState();
        return 'HTML_TAG_ATTR_VALUE';
      %}
<html_tag_attr,html_tag_attr_value>/(([/]{html_tag_del_gt})|{html_tag_del_gt})
      %{
        this.popState();
      %}

/*twig*/
<html_tag_attr_value>{twig_logic_del_open}
      %{
        this.popState();
        this.begin('twig_logic');
        return '\{\%';
      %}
<html,html_text,html_tag_open,html_tag_attr_value_double_quote_string,html_tag_attr_value_single_quote_string>{twig_logic_del_open}
      %{
        this.begin('twig_logic');
        return '\{\%';
      %}
<html_tag_attr_value>{twig_print_del_open}
      %{
        this.popState();
        this.begin('twig_print');
        return '{{';
      %}
<html,html_text,html_tag_open,html_tag_attr_value_double_quote_string,html_tag_attr_value_single_quote_string>{twig_print_del_open}
      %{
        this.begin('twig_print');
        return '{{';
      %}
<twig_logic>'set'
      %{
        return 'set';
      %}
<twig_logic>'for'
      %{
        return 'for';
      %}
<twig_logic>'in'
      %{
        return 'in';
      %}
<twig_logic>'endfor'
      %{
        return 'endfor';
      %}
<twig_logic>'if'
      %{
        return 'if';
      %}
<twig_logic>'else'
      %{
        return 'else';
      %}
<twig_logic>'endif'
      %{
        return 'endif';
      %}
<twig_print,twig_logic>'..'
      %{
        return '..';
      %}
<twig_print,twig_logic>'and'
      %{
        return 'and';
      %}
<twig_print,twig_logic>'or'
      %{
        return 'or';
      %}
<twig_print,twig_logic>'.'
      %{
        return '.';
      %}
<twig_print,twig_logic>'('
      %{
        return '(';
      %}
<twig_print,twig_logic>')'
      %{
        return ')';
      %}
<twig_print,twig_logic>','
      %{
        return ',';
      %}
<twig_print,twig_logic>'['
      %{
        return '[';
      %}
<twig_print,twig_logic>']'
      %{
        return ']';
      %}
<twig_print,twig_logic>';'
      %{
        return ';';
      %}
<twig_print,twig_logic>'+'
      %{
        return '+';
      %}
<twig_print,twig_logic>'-'
      %{
        return '-';
      %}
<twig_print,twig_logic>'*'
      %{
        return '*';
      %}
<twig_print,twig_logic>'/'
      %{
        return '/';
      %}
<twig_print,twig_logic>'?'
      %{
        return '?';
      %}
<twig_print,twig_logic>':'
      %{
        return ':';
      %}
<twig_print,twig_logic>'=='
      %{
        return '==';
      %}
<twig_print,twig_logic>'!='
      %{
        return '!=';
      %}
<twig_print,twig_logic>'='
      %{
        return '=';
      %}
<twig_print,twig_logic>{identifier}
      %{
        return 'TWIG_IDENTIFIER';
      %}
<twig_print,twig_logic>{number}
      %{
        return 'NUMBER';
      %}
<twig_print,twig_logic>[']([^']*)[']
      %{
        yytext = yytext.slice(1,-1);
        return 'TWIG_STRING';
      %}
<twig_print,twig_logic>["]([^"]*)["]
      %{
        yytext = yytext.slice(1,-1);
        return 'TWIG_STRING';
      %}
<twig_logic>{twig_logic_del_close}
      %{
        this.popState();
        return '\%\}';
      %}
<twig_logic>'set'
      %{
        return 'set';
      %}
<twig_print>{twig_print_del_close}
      %{
        this.popState();
        return '}}';
      %}

<INITIAL,html><<EOF>> return 'EOF';
<INITIAL> this.begin('html');
/lex

%left '='
%left '?'
%right ':'
%left '<' '>'
%left 'and' 'or'
%left '==' '!='
%left '+' '-'
%left '*' '/'
%left '^'
%left '~'
%right '!'
%right '%'
%left '.'
%left '[' ']'
%left '(' ')'

%start ast
%% /* language grammar */
ast   : StatementList EOF {
  if(stack.length > 0){
    for(var i=stack.length-1; i>=0; i--){
      if( stack[i].type === 'HtmlElementNode' ){
        console.warn( parseWarnint(yy.lexer.matched,stack[i].loc,'Missing end tag for "<'+stack[i].nodeName+'>"' ) );
      }else{
        console.error( parseError( yy.lexer.matched,stack[i].loc,'Unclosed logical expression {% '+stack[i].nodeName+' %} ' ) );
      }
    }
  }
  $$      = ast;
  ast     = { type:'Root', childNodes: []};
  stack   = [];
  current = ast;
  return $$;
} ;

StatementList
      :                         { $$ = [] }
      | StatementList Statement { $$ = $1.concat($2) }
      ;

Statement
      : HtmlElement
      | PrintStatement
      | Assignment
      | IfStatement
      | ForStatement
      ;

HtmlElement
      : HTML_TEXT                         { createHtmlTextNode(       [@1,@1], $1     ); }
      | '<'  TAG HtmlAttributeList  '>'   { createHtmlElementNode(    [@1,@4], $2, $3 ); }
      | '<'  TAG HtmlAttributeList  '/>'  { createHtmlElement(        [@1,@4], $2, $3 ); }
      | '</' TAG HtmlAttributeList  '>'   { closeHtmlElementNode( yy, [@1,@4], $2, $3 ); }
      ;

HtmlAttributeList
      : HtmlAttributeList HtmlAttribute   { $1.push($2); $$ = $1 }
      |                                   { $$ = [] }
      ;
HtmlAttribute
      : HTML_TAG_ATTR_NAME                          { $$ = { name: $1, value: true} ; }
      | HTML_TAG_ATTR_NAME '=' HTML_TAG_ATTR_VALUE  { $$ = { name: $1, value: $3  } ; }
      ;


Assignment
      : '{%' 'set' identifier '=' Expression '%}'
      ;
IfStatement
      : '{%' 'if' Expression '%}'                   { createIfStatement(   [@1,@4], $3     ) }
      | '{%' 'else' '%}'                            { createIfStatement(   [@1,@3], $3     ) }
      | '{%' 'elseif' Expression '%}'               { createIfStatement(   [@1,@4], $3     ) }
      | '{%' 'endif' '%}'                           { closeIfStatement(    yy, [@1,@3]     ) }
      ;
ForStatement
      : '{%' 'for' Identifier 'in' Expression '%}'                  { createForStatement(  [@1,@6], null, $3, $5 ) }
      | '{%' 'for' Identifier ','  Identifier 'in' Expression '%}'  { createForStatement(  [@1,@7], $3  , $5, $7 ) }
      | '{%' 'for' Identifier 'in' Expression 'if' Expression '%}'  { /*TODO add for if Statement */ }
      | '{%' 'endfor' '%}'                                          { closeForStatement(   yy, [@1,@3]         ) }
      ;
PrintStatement
      : '{{' Expression '}}' { createPrintStatement( [@2,@2], $2 ) }
      | '{{' '}}'
      ;
Expression
      : Identifier
      | BasicDataTypeValue
      | MemberAccessor
      | BinaryOperation
      | UnaryOperation
      | ArrayLiteral
      | CallExpression
      ;
BinaryOperation
      : Expression '+'   Expression { $$ = { loc: createLoc([@1,@3]), type:'operation', operator:$2, left:$1, right:$3 }; }
      | Expression '-'   Expression { $$ = { loc: createLoc([@1,@3]), type:'operation', operator:$2, left:$1, right:$3 }; }
      | Expression '*'   Expression { $$ = { loc: createLoc([@1,@3]), type:'operation', operator:$2, left:$1, right:$3 }; }
      | Expression '/'   Expression { $$ = { loc: createLoc([@1,@3]), type:'operation', operator:$2, left:$1, right:$3 }; }
      | Expression '^'   Expression { $$ = { loc: createLoc([@1,@3]), type:'operation', operator:$2, left:$1, right:$3 }; }
      | Expression '%'   Expression { $$ = { loc: createLoc([@1,@3]), type:'operation', operator:$2, left:$1, right:$3 }; }
      | Expression '=='  Expression { $$ = { loc: createLoc([@1,@3]), type:'operation', operator:$2, left:$1, right:$3 }; }
      | Expression '!='  Expression { $$ = { loc: createLoc([@1,@3]), type:'operation', operator:$2, left:$1, right:$3 }; }
      | Expression 'and' Expression { $$ = { loc: createLoc([@1,@3]), type:'operation', operator:$2, left:$1, right:$3 }; }
      | Expression 'or'  Expression { $$ = { loc: createLoc([@1,@3]), type:'operation', operator:$2, left:$1, right:$3 }; }
      ;
UnaryOperation
      : '+' Expression              { $$ = { loc: createLoc([@1,@2]), type:'uOperation', operator:$1, right:$2 }; }
      | '-' Expression              { $$ = { loc: createLoc([@1,@2]), type:'uOperation', operator:$1, right:$2 }; }
      | '!' Expression              { $$ = { loc: createLoc([@1,@2]), type:'uOperation', operator:$1, right:$2 }; }
      ;
CallExpression
      : Expression '(' ')'                                 { $$ = { loc: createLoc([@1,@3]), type:'call',func:$1,arguments: null }; }
      | Expression '(' ListExpression ')'                  { $$ = { loc: createLoc([@1,@4]), type:'call',func:$1,arguments: $3 }; }
      ;
ArrayLiteral
      : '[' ListExpression ']'                             { $$ = { loc: createLoc([@1,@3]), type:'arrayLiteral', items: $2 }; }
      |  BasicDataTypeValue '..' BasicDataTypeValue        { $$ = { loc: createLoc([@1,@3]), type:'arrayRange'  , from:$1, to:$3 }; }
      ;
ListExpression
      : Expression                    { $$ = [$1]; }
      | ListExpression ',' Expression { $1.push($3); $$ = $1; }
      ;
BasicDataTypeValue
      : NUMBER        { $$ = Number($1); }
      | TWIG_STRING   { $$ = $1; }
      ;
MemberAccessor
      : Expression '.' Identifier     { $$ = { type:'memberAccessor', object:$1, property: $3 };  }
      | Expression '[' Expression ']' { $$ = { type:'memberAccessor', object:$1, property: $3 };  }
      ;
Identifier
      : TWIG_IDENTIFIER { $$ = { type:'Idenrifier', name:$1 }; }
      ;
%%
var ast             = { type:'Root', childNodes: []};
var stack           = [];
var current         = ast;
var singleTag       = {
   'meta' : 1
  ,'link' : 1
  ,'img'  : 1
  ,'input': 1
};

var logicStatements = {
    IfStatement :   {start:'if'    , end:'endif' }
  , ForStatement:   {start:'for'   , end:'endfor'}
  , ElseStatement:  {start:'else'  , end:'endif' }
  , ElseIfStatement:{start:'elseif', end:'endif' }
}

function createRange( start, end ) {
  var ret = null;
  if( typeof start === 'number' && start>=0 &&  typeof end === 'number' && end >= 0 ){
    ret = [];
    for(var i = start; i <= end; i++){
      ret.push(i);
    }
  }else if (typeof start === 'string' && end === 'string'){
    var s = start.charCodeAt(0);
  }
  return ret;
}
function parserMessage( type, src, loc, msg ){
  var _s = Math.max( 0, loc.range[0]-10 );
  var _e = Math.min( src.length, loc.range[1] - _s )
  var tmp = src.substr( _s, _e );
  var message = type + ' on line:'+loc.last_line+' col:'+loc.last_column;
  message    += '\n'+msg;
  message    += '\n'+ (_s?'...':'')+src.substr( _s, _e ).replace('\n\r',' ').replace('\n',' ');
  message    += '\n';
  for(var j=0; j<tmp.length; j++){
    message += '-';
  }
  message += (_s?'--':'')+'^';
  return message;
}
function parseWarnint( src, loc, msg ){
  return parserMessage( '!WARNING! Parse warning ',src, loc, msg );
}
function parseError( src, loc, msg ){
  return parserMessage( '!!ERROR!! Parse error ',src, loc, msg );
}
function createLoc( data ){
  return {
      first_line:   data[0].first_line
    , first_column: data[0].first_column
    , last_line:    data[1].last_line
    , last_column:  data[1].last_column
    , range:        [data[0].range[0],data[1].range[1]]
  }
}

function createPrintStatement( loc, expression ){
  var id = current.childNodes.push({
     loc        : createLoc(loc)
    ,type       : 'PrintStatement'
    ,expression : expression
  });
  return current.childNodes[id-1];
}

function createHtmlTextNode( loc, text ){
  var id = current.childNodes.push({
      loc   : createLoc(loc)
    , type  : 'TextNode'
    , text  : text
  });
  return current.childNodes[id-1];
}

function createHtmlElement( loc, tagName, attributes ){
  var id = current.childNodes.push({
     loc        : createLoc(loc)
   , type       : "HtmlElementNode"
   , nodeName   : tagName
   , attributes : attributes
   , childNodes : []
  });
  return current.childNodes[id-1];
}

function createHtmlElementNode( loc, tagName, attributes ){
  var tmp = createHtmlElement( loc, tagName, attributes );
  if( !singleTag[tagName] ){
    current = tmp;
    stack.push(current);
  }
  return tmp;
}

function closeHtmlElementNode( yy,loc, tagName, attributes ){
  if( singleTag[tagName] ){
    console.warn( parseWarnint(yy.lexer.matched,createLoc(loc),'Single tag element "<'+tagName+'>" used as closing tag ' ) );
    return createHtmlElement(loc,tagName, attributes);
  } else {
    for( var i=stack.length-1; i>=0; i-- ) {
      if( logicStatements[ stack[i].type ] ){
        throw new SyntaxError( parseError(yy.lexer.matched,stack[i].loc,'{% '+stack[i].nodeName+' %} statement not closed with {% end'+stack[i].nodeName+' %} ' ) );
      }
      if( stack[i].nodeName == tagName ) {
        if( stack.length-i > 1 ) {
          for( var k=i+1; k<stack.length; k++){
            console.warn( parseWarnint(yy.lexer.matched,stack[k].loc,'Parent tag was closed before closing child tag "<'+stack[k].nodeName+'>" ' ) );
          }
        }
        stack.splice(i, stack.length-i);
        current = (stack[stack.length-1])?stack[stack.length-1]:ast;
        break;
      }
    }
    if( i === -1 ){
      console.warn( parseWarnint(yy.lexer.matched, createLoc(loc), 'No matching open tag found for closing tag "</'+tagName+'>" ' ) );
    }
  }
}


function createIfStatement( loc, condition ){
  var id = current.childNodes.push({
     loc       : createLoc(loc)
   , type      : "IfStatement"
   , condition : condition
   , then      : []
  });
  current            = current.childNodes[id-1];
  current.childNodes = current.then;
  stack.push(current);
}
function closeIfStatement( yy,loc ){
    for( var i=stack.length-1; i>=0; i-- ) {
        console.log(stack[i].type);
        if( stack[i].type === 'HtmlElementNode'){
          console.warn( parseWarnint(yy.lexer.matched,stack[i].loc,'Trying to close '+stack[i].type+' before closing child tag "<'+stack[i].type.nodeName+'>" ' ) );
        } else if (
                  stack[i].type === 'IfStatement'
                  || (
                        stack[i-1].type === 'IfStatement'
                        && (
                            stack[i].type    === 'ElseStatement'
                            || stack[i].type === 'ElseIfStatement'
                           )
                     )
                  )
        {
            delete stack[i].childNodes;
            stack.splice(i, 1);
            current = stack[stack.length-1];
            break;
        }else{
          throw new SyntaxError( parseError( yy.lexer.matched,stack[i].loc,'Expecting {% ' + logicStatements[ tack[i].type ].end +' %} not yet closed ' ) );
        }
    }
    if( i === -1 ){
      console.warn( parseWarnint(yy.lexer.matched, createLoc(loc), 'IfStatement not opened ' ) );
    }
}


function createForStatement( loc, key, value, items ){
  var id = current.childNodes.push({
     loc       : createLoc(loc)
   , type      : "ForStatement"
   , key       : key
   , value     : value
   , inItems   : items
   , execute   : []
  });
  current            = current.childNodes[id-1];
  current.childNodes = current.execute;
  stack.push(current);
}
function closeForStatement( yy,loc ){
  for( var i=stack.length-1; i>=0; i-- ) {
      if( stack[i].type === 'HtmlElementNode'){
        console.warn( parseWarnint(yy.lexer.matched,stack[i].loc,'Trying to close '+stack[i].type+' before closing child tag "<'+stack[i].type.nodeName+'>" ' ) );
      }else if(
                stack[i].type === 'ForStatement'
                || (
                     stack[i-1].type  === 'ForStatement'
                     && stack[i].type === 'ElseStatement'
                   )
              )
      {
          delete stack[i].childNodes;
          stack.splice(i, 1);
          current = stack[stack.length-1];
          break;
      }else{
        throw new SyntaxError( parseError( yy.lexer.matched,stack[i].loc,'Expecting {% '+logicStatements[ stack[i].type ].end+' %} not yet closed ' ) );
      }
  }
  if( i === -1 ) {
    console.warn( parseWarnint(yy.lexer.matched, createLoc(loc), 'ForStatement not opened ' ) );
  }
}
